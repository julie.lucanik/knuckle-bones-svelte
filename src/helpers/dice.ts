import GiDiceSixFacesOne from 'svelte-icons/gi/GiDiceSixFacesOne.svelte'
import GiDiceSixFacesTwo from 'svelte-icons/gi/GiDiceSixFacesTwo.svelte'
import GiDiceSixFacesThree from 'svelte-icons/gi/GiDiceSixFacesThree.svelte'
import GiDiceSixFacesFour from 'svelte-icons/gi/GiDiceSixFacesFour.svelte'
import GiDiceSixFacesFive from 'svelte-icons/gi/GiDiceSixFacesFive.svelte'
import GiDiceSixFacesSix from 'svelte-icons/gi/GiDiceSixFacesSix.svelte'
import type { DiceT } from '../types'

function getRandomDice(): DiceT {
  return (Math.floor(Math.random() * 6) + 1) as DiceT
}

const diceIcon: Record<DiceT, typeof GiDiceSixFacesOne> = {
  1: GiDiceSixFacesOne,
  2: GiDiceSixFacesTwo,
  3: GiDiceSixFacesThree,
  4: GiDiceSixFacesFour,
  5: GiDiceSixFacesFive,
  6: GiDiceSixFacesSix,
}

function getDiceIcon(dice: DiceT): typeof GiDiceSixFacesOne | null {
  return dice ? diceIcon[dice] : null
}

export { getRandomDice, getDiceIcon }
