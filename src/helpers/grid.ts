import type { ColumnT, BoxT, DiceT, GridT, ColumnKeyT } from '../types'

function areAllDiceEqual(column: ColumnT) {
  return column.every(c => c.dice === column[0].dice)
}

function checkForDuplicates(column: ColumnT, keyName: keyof BoxT) {
  return new Set(column.map(item => item[keyName])).size !== column.length
}

function findDuplicateDice(dices: DiceT[]): DiceT | undefined {
  return dices.find((item, index) => dices.indexOf(item) !== index)
}

function calculateColumn(column: ColumnT): number {
  const filteredColumn = column.filter(c => !!c.dice)

  if (!filteredColumn.length) return 0

  if (filteredColumn.length === 1) return filteredColumn[0].dice

  if (areAllDiceEqual(filteredColumn)) {
    const multiplier = filteredColumn.length === 2 ? 4 : 9
    return filteredColumn[0].dice * multiplier
  }

  const dices = filteredColumn.map(c => c.dice)

  const duplicateDice = findDuplicateDice(dices) // means there are 2 dices in common

  if (duplicateDice) {
    const totalOther = dices.reduce((acc, current) => (current === duplicateDice ? acc : acc + current), 0)

    return duplicateDice * 4 + totalOther
  }

  return dices.reduce((acc, current) => acc + current, 0)
}

function calculateTotalGrid(grid: GridT): number {
  let total: number = 0

  Object.values(grid).forEach(column => {
    const columnTotal = calculateColumn(column)
    total = total + columnTotal
  })

  return total
}

function isGameFinished(grid: GridT): boolean {
  return Object.values(grid).every(col => {
    return col.every(b => !!b.dice)
  })
}

function defineColumn(columnKey: ColumnKeyT): ColumnT {
  return Array.from(Array(3)).map(() => ({ column: columnKey, dice: undefined }))
}

function defineGrid(): GridT {
  const columnKeys: ColumnKeyT[] = [1, 2, 3]

  return columnKeys.reduce((acc, key) => {
    const column = defineColumn(key)
    return { ...acc, [key]: column }
  }, {} as GridT)
}

function getGameWinner(playerOneTotal, playerTwoTotal): 'player1' | 'player2' | 'draw' {
  if (playerOneTotal === playerTwoTotal) return 'draw'

  return playerOneTotal > playerTwoTotal ? 'player1' : 'player2'
}

export {
  areAllDiceEqual,
  calculateColumn,
  calculateTotalGrid,
  checkForDuplicates,
  defineGrid,
  findDuplicateDice,
  getGameWinner,
  isGameFinished,
}
