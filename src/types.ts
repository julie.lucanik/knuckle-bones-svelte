export type ColumnKeyT = 1 | 2 | 3

export type DiceT = 1 | 2 | 3 | 4 | 5 | 6

export type BoxT = {
  column: ColumnKeyT
  dice?: DiceT
}

export type ColumnT = BoxT[]

export type GridT = Record<ColumnKeyT, ColumnT>

export type PlayerKey = 'player1' | 'player2'

export type PlayerT = {
  id: PlayerKey
  name: string
  isPlayerTurn: boolean
  diceInHand: DiceT | null
  grid: GridT
}

export type PlayersStateT = Record<PlayerKey, PlayerT>
