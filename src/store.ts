import { derived, writable } from 'svelte/store'
import { getRandomDice } from './helpers/dice'
import { calculateTotalGrid, defineGrid } from './helpers/grid'
import type { PlayersStateT, PlayerKey, ColumnT, ColumnKeyT, DiceT, BoxT } from './types'

/** ------ PLAYERS ------ */

const defaultPlayersState: PlayersStateT = {
  player1: {
    id: 'player1',
    name: 'Player 1',
    grid: defineGrid(),
    isPlayerTurn: false,
    diceInHand: null,
  },
  player2: {
    id: 'player2',
    name: 'Player 2',
    grid: defineGrid(),
    isPlayerTurn: false,
    diceInHand: null,
  },
}

export const players = writable<PlayersStateT>(defaultPlayersState)

export const playerOneTotal = derived(players, ($players: PlayersStateT) => calculateTotalGrid($players.player1.grid))

export const playerOneName = derived(players, ($players: PlayersStateT) => $players.player1.name)

export const playerTwoName = derived(players, ($players: PlayersStateT) => $players.player2.name)

export const playerTwoTotal = derived(players, ($players: PlayersStateT) => calculateTotalGrid($players.player2.grid))

function getOpponentKey(currentPlayerId: PlayerKey): PlayerKey {
  return currentPlayerId === 'player1' ? 'player2' : 'player1'
}

export function switchPlayersTurn(currentPlayerId: PlayerKey) {
  players.update(state => {
    const cloneState = { ...state }
    const opponent: PlayerKey = getOpponentKey(currentPlayerId)

    cloneState[currentPlayerId] = { ...cloneState[currentPlayerId], diceInHand: null, isPlayerTurn: false }
    cloneState[opponent] = { ...cloneState[opponent], diceInHand: getRandomDice(), isPlayerTurn: true }

    return cloneState
  })
}

function updateColumn(column: ColumnT, dice: DiceT): ColumnT {
  const emptyBoxIndex = column.findIndex((box: BoxT) => !Boolean(box.dice))

  return column.map((b, idx) => (idx === emptyBoxIndex ? { ...b, dice } : b))
}

function cleanSameColumnDice(column: ColumnT, dice: DiceT): ColumnT {
  const filteredColumns = column.filter(b => b.dice && b.dice !== dice)
  const emptyColumns: ColumnT = Array(3 - filteredColumns.length).fill({ column: column[0].column, dice: undefined })

  return [...filteredColumns, ...emptyColumns]
}

export function updateGrid(playerId: PlayerKey, dice: DiceT, columnKey: ColumnKeyT) {
  players.update(state => {
    const cloneState = { ...state }
    const columnToUpdate: ColumnT = [...cloneState[playerId].grid[columnKey]]

    cloneState[playerId].grid[columnKey] = updateColumn(columnToUpdate, dice)

    const opponent: PlayerKey = getOpponentKey(playerId)
    const opponentColumnToUpdate: ColumnT = [...cloneState[opponent].grid[columnKey]]

    cloneState[opponent].grid[columnKey] = cleanSameColumnDice(opponentColumnToUpdate, dice)

    return cloneState
  })
}

/** ------ GAME STATES ------ */

export const hasGameEnded = writable<boolean>(false)
export const isNewGameStarted = writable<boolean>(false)

export function startGame(): void {
  isNewGameStarted.update(() => true)
  hasGameEnded.update(() => false)

  // reset players grid
  players.update(() => {
    return {
      ...defaultPlayersState,
      player1: { ...defaultPlayersState.player1, isPlayerTurn: true, diceInHand: getRandomDice() },
    }
  })
}

export function toggleGameOver(gameOver?: boolean): void {
  hasGameEnded.update(prev => gameOver ?? !prev)
  isNewGameStarted.update(() => false)
}
